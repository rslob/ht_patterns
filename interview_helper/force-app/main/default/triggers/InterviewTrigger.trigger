trigger InterviewTrigger on Interview__c (after insert, after update) {
    //Implemented using 'Triggers of Awesome'
    //For details, please visit https://github.com/codefriar/DecouplingWithSimonGoodyear/
    new CustomMDTTriggerHandler().run();
}