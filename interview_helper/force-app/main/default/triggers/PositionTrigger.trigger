trigger PositionTrigger on Position__c (after update) {
    //Implemented using 'Triggers of Awesome'
    //For details, please visit https://github.com/codefriar/DecouplingWithSimonGoodyear/
    new CustomMDTTriggerHandler().run();
}