public with sharing class LegacySystemDTO {
    //Some fields to send to external system (sample)
    public String interviewId;
    public String interviewTitle;
    public Integer questionsAsked;
}
