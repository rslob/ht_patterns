@isTest
public with sharing class IntegrationWithLegasySystemTest {
    
    @TestSetup
    static void makeData(){
        List <SObject> recordsToInset = new List <SObject> ();

        //Add contact
        Contact testContact = new Contact (
            FirstName = 'John',
            LastName = 'Doe'
        );
        insert testContact;

        //Add open position
        Position__c position = new Position__c (
            Position_Title__c = 'Salesforce Developer',
            Expected_Maturity_Level__c = 'Senior',
            Approach_Technology__c = 'Salesforce Developer'
        );
        insert position;

        //Add interview
        recordsToInset.add(new Interview__c (
            Interviewer__c = UserInfo.getUserId(),
            Interviewee__c = testContact.Id,
            Position__c = position.Id
        ));

        insert recordsToInset;
    }

    @isTest
    static void testIntegrationWithLegasySystem () {
        //Create instance
        InterviewDetails intDetails = new InterviewDetails ();
        //Send details
        intDetails.sendDetails(new List <Interview__c> ([SELECT Id, Position_Title__c FROM Interview__c]));
    }
}
