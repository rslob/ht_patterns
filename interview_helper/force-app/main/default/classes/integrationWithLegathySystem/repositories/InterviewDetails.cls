//Integration class (fake) which implemented using Adapter
public with sharing class InterviewDetails {

    public InterviewDetails () {
        //Empty constructor
    }

    //Send interviews details to legacy system using REST API (fake)
    public void sendDetails (List <Interview__c> interviewsToSend) {
        //Convert list of interviews to map 
        Map <Id, Interview__c> interviewsToSendAsMap = new Map <Id, Interview__c> (interviewsToSend);
        
        //Agregate data
        List <LegacySystemDTO> interviewsInExternalForm = new List <LegacySystemDTO> ();
        List <Interview_Question__c> relatedInterviewQuestions = new List <Interview_Question__c> ();
        Map <Id, Integer> questionsCountPerInterview = new Map <Id, Integer> ();
        relatedInterviewQuestions = [SELECT Id, Interview__c 
                                     FROM Interview_Question__c
                                     WHERE Interview__c 
                                     IN :interviewsToSendAsMap.keySet()];
        for (Interview_Question__c interviewQuestion : relatedInterviewQuestions) {
            Id interviewId = interviewQuestion.Interview__c;
            Integer questionsCount = 0;
            if (questionsCountPerInterview.containsKey(interviewId)) {
                questionsCount = questionsCountPerInterview.get(interviewId);
            }
            questionsCount++;
            questionsCountPerInterview.put(interviewId, questionsCount);
        }

        //Convert data to external form
        for (Interview__c interviewToSend : interviewsToSend) {
            Id interviewId = interviewToSend.Id;
            LegacySystemDTO wrappedInterview = new LegacySystemDTO ();
            wrappedInterview.interviewId = interviewToSend.Id;
            wrappedInterview.interviewTitle = interviewToSend.Position_Title__c;
            wrappedInterview.questionsAsked = questionsCountPerInterview.get(interviewId);
            interviewsInExternalForm.add(wrappedInterview);
        }
        //Send data to external system (not implemented)
    }
}
