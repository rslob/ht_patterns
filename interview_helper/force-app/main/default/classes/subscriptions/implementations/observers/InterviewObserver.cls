public with sharing class InterviewObserver implements IObserver {

    //Utilities
    private USubscription SubscriptionUtils;
    private ConstantsManager ConstManager;

    //Constructor
    public InterviewObserver () {
        //Init utils for future use
        this.SubscriptionUtils = new USubscription ();
        this.ConstManager = new ConstantsManager ();
    } 

    public void notify (List <SObject> changedSubjects) {
        //Group changed subjects by their Object Type
        Map <String, List <SObject>> changedSubjectsByObjType = new Map <String, List <SObject>> ();
        for (SObject changedSubject : changedSubjects) {
            System.debug(changedSubject);
            String recId = (String) changedSubject.get('Id');
            String sObjType = this.SubscriptionUtils.getSObjectAPINameByRecordId(recId);
            List <SObject> subjectsOfGivenType;
            if (changedSubjectsByObjType.containsKey(sObjType)) {
                subjectsOfGivenType = changedSubjectsByObjType.get(sObjType);
            } else {
                subjectsOfGivenType = new List <SObject> ();
            }
            subjectsOfGivenType.add(changedSubject);
            changedSubjectsByObjType.put(sObjType, subjectsOfGivenType);
        }

        //Execute subject-specific business logic
        for (String sObjType : changedSubjectsByObjType.keySet()) {
            switch on sObjType {
                when 'Position__c' {
                    //Void interivew when Position is closed
                    voidInterviewOnPositionClosure((List <Position__c>) changedSubjectsByObjType.get(sObjType));
                }
                when else {
                    //Ignore for now
                }
            }
        }
    }

    private void voidInterviewOnPositionClosure (List <Position__c> changedPositions) {
        //Constants
        String voidPositionStatus = this.ConstManager.getConstantValue('IH_Constant__mdt', 'VOID_POSITION_STATUS');
        String voidInterviewStatus = this.ConstManager.getConstantValue('IH_Constant__mdt', 'VOID_INTERVIEW_STATUS');

        //Convert list of changed positions to map
        Map <Id, Position__c> changedPositionsMap = new Map <Id, Position__c> (changedPositions);
        
        //Filter out not void positions
        for (Id positionId : changedPositionsMap.keySet()) {
            Position__c position = changedPositionsMap.get(positionId);
            String positionStatus = position.Status__c;
            if (String.isNotBlank(positionStatus)
                && positionStatus != voidPositionStatus
                && positionStatus.containsNone('Closed - ')) {
                changedPositionsMap.remove(positionId);
            }
        }
        
        //Get interviews which are not yet void
        List <Interview__c> interviews = new List <Interview__c> ();
        interviews = [SELECT Id 
                      FROM Interview__c 
                      WHERE Status__c != :voidInterviewStatus
                      AND Position__c IN :changedPositionsMap.keySet()];
        
        //Update interview status
        for (Interview__c interview : interviews) {
            interview.Status__c = voidInterviewStatus;
        }
        update interviews;
    }
}
