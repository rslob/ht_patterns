//Generic class to manage subscriptions of any-to-any objects.
//Implemented using Pub/Sub pattern
public with sharing class GenericSubject implements ISubject {
    //Used to store context for each instance of given class
    private List <Subscription__c> Subscribers;
    //Utilities
    private USubscription SubscriptionUtils;

    //Default constructor
    public GenericSubject () {
        //Init utilities
        this.SubscriptionUtils = new USubscription ();
        //Init list of subscribers
        this.Subscribers = new List <Subscription__c> ();
    }

    //Custom constructor
    //Should be used to make subscribe/unsubscribe functionality working propertly
    public GenericSubject (List <Subscription__c> subs) {
        //Call default constructor to init utilitie
        this();
        //Init list of Subscriptions with argument's value (if not empty)
        if (subs != null && subs.size() > 0) {
            this.Subscribers = subs;
        }
    }

    public void subscribe () {
        List <Subscription__c> newSubs = new List <Subscription__c> ();
        //Check are there any subs with given arguments and delete them from subs to be inserted
        Map <String, Subscription__c> subsByIdsConcat = new Map <String, Subscription__c> ();
        for (Subscription__c subscriber : this.Subscribers) {
            subsByIdsConcat.put(subscriber.Subject_Id__c + '' + subscriber.Observer_Id__c, subscriber);
        }
        List <Subscription__c> existingSubs = [SELECT Id, Subject_Id__c, Observer_Id__c, Subject_and_Observer__c
                                               FROM Subscription__c
                                               WHERE Subject_and_Observer__c
                                               IN :subsByIdsConcat.keySet()];
        for (Subscription__c existingSub : existingSubs) {
            subsByIdsConcat.remove(existingSub.Subject_and_Observer__c);
        }
        for (String mKey : subsByIdsConcat.keySet()) {
            Subscription__c subToAdd = subsByIdsConcat.get(mKey);
            subToAdd.Observer_Object_API_Name__c = this.SubscriptionUtils.getSObjectAPINameByRecordId(subToAdd.Observer_Id__c);
            subToAdd.Subject_Object_API_Name__c = this.SubscriptionUtils.getSObjectAPINameByRecordId(subToAdd.Subject_Id__c);
            newSubs.add(subToAdd);
        }
        //Insert subs which not exists (if any)
        if (newSubs.size() > 0) {
            insert newSubs;
        }
    }

    public void unsubscribe () {
        List <Subscription__c> subsToDelete = new List <Subscription__c> ();
        //Get subscriptions to delete
        List <String> idsConcat = new List <String> ();
        for (Subscription__c subscriber : this.Subscribers) {
            idsConcat.add(subscriber.Subject_Id__c + '' + subscriber.Observer_Id__c);
        }
        subsToDelete = [SELECT Id
                        FROM Subscription__c
                        WHERE Subject_and_Observer__c
                        IN :idsConcat];
        //Delete subs which not exists (if any)
        if (subsToDelete.size() > 0) {
            delete subsToDelete;
        }
    }

    public void notify (List <SObject> impactedSubjects) {
        //Convert impactedSubjects to map
        Map <Id, SObject> impactedSubjectsMap = new Map <Id, SObject> ();
        for (SObject impactedSubject : impactedSubjects) {
            impactedSubjectsMap.put((Id) impactedSubject.get('Id'), impactedSubject);
        }
        //Get subscriptions to notify observers
        List <Subscription__c> subscriptions = [SELECT Id, Subject_Id__c, Subject_Object_API_Name__c, Observer_Id__c, Observer_Object_API_Name__c
                                                FROM Subscription__c
                                                WHERE Subject_Id__c IN :impactedSubjectsMap.keySet()];
        //Grouping subscriptions by observer's object type
        Map <String, List <Subscription__c>> subscriptionsByObsAPIName = new Map <String, List <Subscription__c>> ();
        for (Subscription__c subscription : subscriptions) {
            String obsAPIName = subscription.Observer_Object_API_Name__c;
            List <Subscription__c> subs;
            if (subscriptionsByObsAPIName.containsKey(obsAPIName)) {
                subs = subscriptionsByObsAPIName.get(obsAPIName);
                subs.add(subscription);
            } else {
                subs = new List <Subscription__c> {subscription};
            }
            subscriptionsByObsAPIName.put(obsAPIName, subs);
        }
        //Notify observers
        for (String obsAPIName : subscriptionsByObsAPIName.keySet()) {
            //Used set to avoid duplicates
            Set <String> specificSubjectsIds = new Set <String> ();
            List <SObject> specificSubjects = new List <SObject> ();
            for (Subscription__c subscription : subscriptionsByObsAPIName.get(obsAPIName)) {
                String specificSubjectId = subscription.Subject_Id__c;
                if (!specificSubjectsIds.contains(specificSubjectId)) {
                    specificSubjectsIds.add(specificSubjectId);
                    specificSubjects.add(impactedSubjectsMap.get(specificSubjectId));
                }
            }
            switch on obsAPIName {
                when 'Interview__c' {
                    interviewObserver intObs = new interviewObserver ();
                    intObs.notify(specificSubjects);
                }
                when else {
                    //Ignore for now
                }
            }
        }
    }
}
