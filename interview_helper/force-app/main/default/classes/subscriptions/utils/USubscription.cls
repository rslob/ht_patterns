//Utility class for subscriptions functionality
//Implemented using Proxy pattern 
public with sharing class USubscription {

    //Used to avoid extra usage of limits during retrieval of Object's API Names by records' IDs
    private Map <String, String> IdToSObjMap;
    
    //Constructor
    public USubscription() {
        this.IdToSObjMap = new Map <String, String> ();
    }

    //Helper/utility method, which implemented using Proxy
    public String getSObjectAPINameByRecordId (String recordId) {
        String result = '';
        String objId = recordId.left(3);
        if (this.IdToSObjMap.containsKey(objId)) {
            result = this.IdToSObjMap.get(objId);
        } else {
            result = Id.valueOf(recordId).getSobjectType().getDescribe().getName();
            this.IdToSObjMap.put(objId, result);
        }
        return result;
    }
}
