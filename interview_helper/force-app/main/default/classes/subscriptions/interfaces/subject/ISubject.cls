public interface ISubject {
    void subscribe ();
    void unsubscribe ();
    void notify (List <SObject> impactedSubjects);
}
