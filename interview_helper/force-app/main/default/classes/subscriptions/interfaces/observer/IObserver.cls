public interface IObserver {
    void notify (List <SObject> changedSubjects);
}
