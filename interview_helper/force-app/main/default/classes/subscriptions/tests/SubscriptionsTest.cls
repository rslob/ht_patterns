@isTest
public class SubscriptionsTest {
    
    @TestSetup
    static void makeData() {
        List <SObject> recordsToInset = new List <SObject> ();
        //Init Constants manager
        ConstantsManager constManager = new ConstantsManager ();

        //Add contact
        Contact testContact = new Contact (
            FirstName = 'John',
            LastName = 'Doe'
        );
        recordsToInset.add(testContact);

        //Add open, closed and void positions
        List <Position__c> positions = new List <Position__c> ();
        //Open position
        positions.add(new Position__c (
            Position_Title__c = 'Salesforce Developer',
            Expected_Maturity_Level__c = 'Senior',
            Approach_Technology__c = 'Salesforce Developer'
        ));
        //Closed position
        positions.add(new Position__c (
            Status__c = constManager.getConstantValue('IH_Constant__mdt', 'CLOSED_CF_POSITION_STATUS'),
            Position_Title__c = 'Salesforce Developer',
            Expected_Maturity_Level__c = 'Senior',
            Approach_Technology__c = 'Salesforce Developer'
        ));
        //Void position
        positions.add(new Position__c (
            Status__c = constManager.getConstantValue('IH_Constant__mdt', 'VOID_POSITION_STATUS'),
            Position_Title__c = 'Salesforce Developer',
            Expected_Maturity_Level__c = 'Senior',
            Approach_Technology__c = 'Salesforce Developer'
        ));
        recordsToInset.addAll(positions);

        insert recordsToInset;
    }

    @isTest
    static void testInterviewObserver () {
        //Init Constants manager
        ConstantsManager constManager = new ConstantsManager ();

        //Get data
        Contact testContact = [SELECT Id FROM Contact LIMIT 1];
        List <Position__c> positions = [SELECT Id FROM Position__c];
        List <String> interviewStatuses = new List <String> {
            'New',
            constManager.getConstantValue('IH_Constant__mdt', 'CLOSED_INTERVIEW_STATUS'),
            constManager.getConstantValue('IH_Constant__mdt', 'VOID_INTERVIEW_STATUS')
        };

        //Create new interviews and check subsciriptions logic
        //Only 1 subscription (Open Interview to Open Position) should exsit
        List <Interview__c> interviews = new List <Interview__c> ();
        for (Position__c position : positions) {
            for (String interviewStatus : interviewStatuses) {
                interviews.add(new Interview__c (
                    Status__c = interviewStatus,
                    Interviewer__c = UserInfo.getUserId(),
                    Interviewee__c = testContact.Id,
                    Position__c = position.Id
                ));
            }
        }
        insert interviews;
        System.assertEquals(1, [SELECT COUNT() FROM Subscription__c]);

        //Verify that interviews (observers) are notified when positions (subjects) are updated
        //Existing subscription should be deleted since all positions are moved to void status
        for (Position__c position : positions) {
            position.Status__c = constManager.getConstantValue('IH_Constant__mdt', 'VOID_POSITION_STATUS');
        }
        update positions;
        System.assertEquals(0, [SELECT COUNT() FROM Subscription__c]);
    }
}
