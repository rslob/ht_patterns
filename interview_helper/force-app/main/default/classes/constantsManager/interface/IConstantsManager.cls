public interface IConstantsManager {
    String getConstantValue(String customMetadataTypeName, String constantDeveloperName);
}
