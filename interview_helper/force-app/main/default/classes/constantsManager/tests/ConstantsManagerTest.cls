@isTest
public with sharing class ConstantsManagerTest {
    
    @isTest
    static void testConstantsManager () {
        //Init manager
        ConstantsManager constManager = new ConstantsManager ();

        //Getting constant value using manager (cold launch)
        String constantValue = constManager.getConstantValue('IH_Constant__mdt', 'CLOSED_INTERVIEW_STATUS');

        //Getting constant value using manager (hot launch, proxy test)
        constantValue = constManager.getConstantValue('IH_Constant__mdt', 'CLOSED_INTERVIEW_STATUS');

        //No queries should be executed
        System.assertEquals(0, Limits.getAggregateQueries());
    }
}
