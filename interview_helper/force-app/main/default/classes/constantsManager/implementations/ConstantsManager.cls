//Implemented using Proxy
public with sharing class ConstantsManager implements IConstantsManager {
    
    private static Map <String, Map<String, String>> constants = new Map <String, Map <String, String>> ();
    
    public String getConstantValue (String customMetadataTypeName, String constantDeveloperName) {
        String constantValue = '';
        Map <String, String> constantsOfMetadataType;
        if (constants.containsKey(customMetadataTypeName) && constants.get(customMetadataTypeName).containsKey(constantDeveloperName)) {
            constantValue = constants.get(customMetadataTypeName).get(constantDeveloperName);
        } else {
            SObject returnedObj = Database.query('SELECT Value__c ' + 
                                                 'FROM ' + customMetadataTypeName + ' ' +
                                                 'WHERE DeveloperName = :constantDeveloperName ' +
                                                 'LIMIT 1');
            constantValue = (String) returnedObj.get('Value__c');
            if (constants.containsKey(customMetadataTypeName)) {
                constantsOfMetadataType = constants.get(customMetadataTypeName);
            } else {
                constantsOfMetadataType = new Map <String, String> ();
            }
            constantsOfMetadataType.put(constantDeveloperName, constantValue);
            constants.put(customMetadataTypeName, constantsOfMetadataType);
        }
        return constantValue;
    }
}
