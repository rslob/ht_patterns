@isTest
public class QuestionsLinkingTest {
    
    @TestSetup
    static void makeData() {
        //Get values from Global Values Sets and convert them to String using workaround.
        //For more details visit https://salesforce.stackexchange.com/questions/147004/retrive-globalpicklist-globalvalueset-without-soql
        SObjectField maturityLevelsField = Question__c.Maturity_Level__c;
        SObjectField applicableApproachTechnologyField = Question__c.Applicable_Approach_Technology__c;
        List <PicklistEntry> maturityLevelsPEs = maturityLevelsField.getDescribe().getPicklistValues();
        List <PicklistEntry> approachesAndTechnologiesPEs = applicableApproachTechnologyField.getDescribe().getPicklistValues();
        String maturityLevels = '';
        String approachesAndTechnologies = '';
        for (PicklistEntry pe : maturityLevelsPEs) {
            maturityLevels += pe.getValue() + ';';
        }
        for (PicklistEntry pe : approachesAndTechnologiesPEs) {
            approachesAndTechnologies += pe.getValue() + ';';
        }
        maturityLevels = maturityLevels.removeEnd(';');
        approachesAndTechnologies = approachesAndTechnologies.removeEnd(';');

        List <SObject> recordsToInsert = new List <SObject> ();

        //Add contact
        Contact testContact = new Contact (
            FirstName = 'John',
            LastName = 'Doe'
        );
        recordsToInsert.add(testContact);

        //Add positions
        List <Position__c> positions = new List <Position__c> ();
        for (PicklistEntry maturityLevelPE : maturityLevelsPEs) {
            positions.add(new Position__c (
                Position_Title__c = 'Salesforce Developer',
                Expected_Maturity_Level__c = maturityLevelPE.getValue(),
                Approach_Technology__c = 'Salesforce Developer'
            ));
        }
        recordsToInsert.addAll(positions);

        //Add questions
        List <Question__c> questions = new List <Question__c> ();
        questions.add(new Question__c (
            Question_Priority__c = 100,
            Question_Text__c = 'What is your name?',
            Required__c = 'Yes',
            Maturity_Level__c = maturityLevels,
            Applicable_Approach_Technology__c = approachesAndTechnologies
        ));
        questions.add(new Question__c (
            Question_Priority__c = 99,
            Question_Text__c = 'Have you ever worked in our Company?',
            Required__c = 'Yes',
            Maturity_Level__c = maturityLevels.removeStart('Trainee;'),
            Applicable_Approach_Technology__c = approachesAndTechnologies
        ));
        recordsToInsert.addAll(questions);

        insert recordsToInsert;
    }

    @isTest
    static void testBehaviors () {
        //Get data
        Contact testContact = [SELECT Id FROM Contact LIMIT 1];
        List <Position__c> positions = [SELECT Id FROM Position__c];

        //Add interviews for positions to test logic in trigger handler
        List <Interview__c> interviews = new List <Interview__c> ();
        for (Position__c position : positions) {
            interviews.add(new Interview__c (
                Interviewer__c = UserInfo.getUserId(),
                Interviewee__c = testContact.Id,
                Position__c = position.Id
            ));
        }
        insert interviews;

        System.assertEquals(11, [SELECT COUNT() FROM Interview_Question__c]);
    }
}
