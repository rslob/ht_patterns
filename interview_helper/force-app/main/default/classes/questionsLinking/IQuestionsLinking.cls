public interface IQuestionsLinking {
    List <Question__c> getQuestionsToLink (List <Interview__c> interviews, List <Question__c> questions);
}
