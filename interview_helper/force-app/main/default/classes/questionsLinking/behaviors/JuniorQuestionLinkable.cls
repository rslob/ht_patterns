public class JuniorQuestionLinkable implements IQuestionsLinking {
    public List <Question__c> getQuestionsToLink (List <Interview__c> interviews, List <Question__c> questions) {
        System.debug('Linking questions for Junior');
        List <Question__c> questionsToLink = new List <Question__c> ();
        for (Question__c question : questions) {
            if (
                question.Maturity_Level__c.contains('Trainee') ||
                question.Maturity_Level__c.contains('Junior')
            ) {
                questionsToLink.add(question);
            }
        }
        return questionsToLink;
    }
}
