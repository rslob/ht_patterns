public class ClusterLeadQuestionLinkable implements IQuestionsLinking {
    public List <Question__c> getQuestionsToLink (List <Interview__c> interviews, List <Question__c> questions) {
        System.debug('Linking questions for ClusterLead');
        List <Question__c> questionsToLink = new List <Question__c> ();
        for (Question__c question : questions) {
            if (
                question.Maturity_Level__c.contains('Trainee') ||
                question.Maturity_Level__c.contains('Junior') ||
                question.Maturity_Level__c.contains('Intermediate') ||
                question.Maturity_Level__c.contains('Senior') ||
                question.Maturity_Level__c.contains('Architect') ||
                question.Maturity_Level__c.contains('Cluster Lead')
            ) {
                questionsToLink.add(question);
            }
        }
        return questionsToLink;
    }
}
