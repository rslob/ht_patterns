public class InterviewTriggerHandlerHelper {

    //Constants manager
    private ConstantsManager ConstManager;

    //Constants
    private final String voidInterviewStatus;
    private final String voidPositionStatus;
    private final String closedInterviewStatus;

    public InterviewTriggerHandlerHelper () {
        this.ConstManager = new ConstantsManager ();
        this.voidInterviewStatus = this.ConstManager.getConstantValue('IH_Constant__mdt', 'VOID_INTERVIEW_STATUS');
        this.voidPositionStatus = this.ConstManager.getConstantValue('IH_Constant__mdt', 'VOID_POSITION_STATUS');
        this.closedInterviewStatus = this.ConstManager.getConstantValue('IH_Constant__mdt', 'CLOSED_INTERVIEW_STATUS');
    }

    //Implementation of automated questions linking feature using 'Strategy' and 'Factory'
    public void linkQuestions (List <Interview__c> interviewsToMap) {
        //Get all required questions
        List <Question__c> allRequiredQuestions = [
            SELECT Id, Applicable_Approach_Technology__c, Maturity_Level__c
            FROM Question__c
            WHERE Required__c = 'Yes'
            ORDER BY Question_Priority__c DESC
        ];

        //Create map of interviews based on position's maturity level
        Map <String, List <Interview__c>> interviewsByMaturityMap = new Map <String, List <Interview__c>> ();
        for (Interview__c interview : interviewsToMap) {
            if (interviewsByMaturityMap.containsKey(interview.Expected_Maturity_Level__c)) {
                List <Interview__c> interviews = interviewsByMaturityMap.get(interview.Expected_Maturity_Level__c);
                interviews.add(interview);
                interviewsByMaturityMap.put(interview.Expected_Maturity_Level__c, interviews);
            } else {
                interviewsByMaturityMap.put(interview.Expected_Maturity_Level__c, new List <Interview__c> {interview});
            }
        }

        //Get questions to link depending on position's maturity level and link them to interview
        List <Interview_Question__c> interviewQuestions = new List <Interview_Question__c> ();
        for (String maturityLevel : interviewsByMaturityMap.keySet()) {
            List <Interview__c> interviews = interviewsByMaturityMap.get(maturityLevel);
            String className = maturityLevel.replace(' ', '') + 'QuestionLinkable';
            System.debug('className: ' + className);
            //Using factory
            IQuestionsLinking specificQLI = (IQuestionsLinking) Type.forName(className).newInstance();
            List <Question__c> questionsToLink = specificQLI.getQuestionsToLink(interviews, allRequiredQuestions);
            for (Interview__c interview : interviews) {
                for (Question__c question : questionsToLink) {
                    if (question.Applicable_Approach_Technology__c.contains(interview.Approach_Technology__c)) {
                        interviewQuestions.add(new Interview_Question__c (
                            Interview__c = interview.Id,
                            Question__c = question.Id
                        ));
                    }
                }
            }
        }
        insert interviewQuestions;
    }

    //Implementation of automated Interviews subscription to parent's Position updates
    public void subscribeInterviewsToPositions (List <Interview__c> interviews) {
        //Get related positions
        Set <Id> relatedPositionsIds = new Set <Id> ();
        List <Position__c> relatedPositions = new List <Position__c> ();
        for (Interview__c interview : interviews) {
            relatedPositionsIds.add(interview.Position__c);
        }
        relatedPositions = [SELECT Id, Status__c FROM Position__c WHERE Id IN :relatedPositionsIds];

        //Convert list of related positions to map of position statuses by poistion Id
        Map <Id, String> posStatusById = new Map <Id, String> ();
        for (Position__c position : relatedPositions) {
            posStatusById.put(position.Id, position.Status__c);
        }

        //Subscribe not closed and not void interviews to not closed or not void positions' updates
        List <Subscription__c> subscribers = new List <Subscription__c> ();
        for (Interview__c interview : interviews) {
            //Filter void and closed interviews or interviews which are lined to closed or void positions
            String relatedPositionStatus = posStatusById.get(interview.Position__c);
            String interviewStatus = interview.Status__c;
            if (this.isPositionInOpenStatuses(relatedPositionStatus) 
                && this.isInterviewInOpenStatuses(interviewStatus)) {
                subscribers.add(new Subscription__c(
                    Subject_Id__c = interview.Position__c,
                    Observer_Id__c = interview.Id
                ));
            }
        }

        GenericSubject gs = new GenericSubject(subscribers);
        gs.subscribe();
    }

    //Implementation of automated Interviews unsubscription from parent's Position updates
    public void unsubscribeInterviewsFromPositions (List <Interview__c> interviews) {
        //Unsubscribe closed or void interviews from positions' updates
        List <Subscription__c> subscribers = new List <Subscription__c> ();
        for (Interview__c interview : interviews) {
            if (!this.isInterviewInOpenStatuses(interview.Status__c)) {
                subscribers.add(new Subscription__c (
                    Observer_Id__c = interview.Id,
                    Subject_Id__c = interview.Position__c
                ));
            }
        }

        GenericSubject gs = new GenericSubject(subscribers);
        gs.unsubscribe();
    }

    //Helper method
    private Boolean isPositionInOpenStatuses (String positionStatus) {
        Boolean isOpen;
        if (String.isNotBlank(positionStatus)
            && (positionStatus == this.voidPositionStatus 
                || positionStatus.contains('Closed - '))) {
            isOpen = false;
        } else {
            isOpen = true;
        }
        return isOpen;
    }

    //Helper method
    private Boolean isInterviewInOpenStatuses (String interviewStatus) {
        Boolean isOpen;
        if (String.isNotBlank(interviewStatus)
            && (interviewStatus == this.closedInterviewStatus
                || interviewStatus == this.voidInterviewStatus)) {
            isOpen = false;
        } else {
            isOpen = true;
        }
        return isOpen;
    }
}
