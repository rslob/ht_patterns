public with sharing class InterviewTriggerHandler extends TriggerHandler {

    //Implemented using 'Triggers of Awesome'
    //For details, please visit https://github.com/codefriar/DecouplingWithSimonGoodyear/

    private List <Interview__c> newInterviews;
    private List <Interview__c> oldInterviews;

    public InterviewTriggerHandler () {
        this.newInterviews = Trigger.new;
        this.oldInterviews = Trigger.old;
    }

    public override void afterInsert () {
        //Init helper
        InterviewTriggerHandlerHelper ithHelper = new InterviewTriggerHandlerHelper ();

        //Link questions
        ithHelper.linkQuestions(this.newInterviews);

        //Subscribe interviews to parent products
        ithHelper.subscribeInterviewsToPositions(this.newInterviews);
    }

    public override void afterUpdate () {
        //Init helper
        InterviewTriggerHandlerHelper ithHelper = new InterviewTriggerHandlerHelper ();
        
        //Unsubscribe interviews from parent products when interviews are void
        ithHelper.unsubscribeInterviewsFromPositions(this.newInterviews);
    }
}
