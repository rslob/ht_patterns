public with sharing class PositionTriggerHandlerHelper {
    public PositionTriggerHandlerHelper() {
        //Empty constructor
    }

    public static void notifyObservers (List <Position__c> changedPositions) {
        GenericSubject gs = new GenericSubject();
        gs.notify(changedPositions);
    }
}
