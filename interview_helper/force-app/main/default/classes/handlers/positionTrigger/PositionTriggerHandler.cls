public with sharing class PositionTriggerHandler extends TriggerHandler {
    
    //Implemented using 'Triggers of Awesome'
    //For details, please visit https://github.com/codefriar/DecouplingWithSimonGoodyear/
    
    private List <Position__c> newPositions;
    private List <Position__c> oldPositions;

    public PositionTriggerHandler () {
        this.newPositions = Trigger.new;
        this.oldPositions = Trigger.old;
    }

    public override void afterUpdate () {
        //Notify observers
        PositionTriggerHandlerHelper.notifyObservers(this.newPositions);
    }
}
